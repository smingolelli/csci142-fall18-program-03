package connectmodel;

import java.util.Vector;
/**
 * Player class assigns the name and color of that player's piecetype.
 * 
 * Only certain names can be used, and if an invalid name
 * is passed through it will be set to the default name.
 * 
 * It also increments scores as well as keeps track of them.
 * 
 * @author SMING, DPLANTE
 *
 */
public class Player 

{
	public static final String DEFAULT_NAME = "JohnCena";
	private String myName;
	private int myNumWins;
	private PieceType myPieceType;
	private Vector<Object> invalidCharacters = new Vector<Object>();
	
	

	public Player(String name, PieceType type)
	{
		myName = name;
		
		myPieceType = type;
	  
	}

	
	/**
	 * Checks if the characters in a name are valid or not, using
	 * a String name.
	 * 
	 * Special characters, underscores, periods, spaces, etc are 
	 * not allowed. If the validateName method finds one, it will set
	 * the name to DEFAULT_NAME
	 * 
	 * @param name
	 * @return
	 */
	private boolean validateName(String name) 
	{
		/*
		 * This will use the unicode table of characters to fill a
		 * vector with special characters and a space.
		 */
		for(int i = 32; i <= 47; i++)
		{
			invalidCharacters.add((char)(i));
		}
		
		for(int i = 58; i <= 64; i++)
		{
			invalidCharacters.add((char)(i));
		}
		
		for(int i = 91; i <= 96; i++)
		{
			invalidCharacters.add((char)(i));
		}
		
		for(int i = 123; i <= 126; i++)
		{
			invalidCharacters.add((char)(i));
		}
		
		
		/*
		 * A nested for loop that checks characters in a name.
		 * 
		 * If a character that is in the invalidCharacters vector is used
		 * it will instead set the name to DEFAULT_NAME.
		 */
		for(int i = 0; i < name.length(); i++)
		{
			for(int j = 0; j <invalidCharacters.size(); j++)
			{
				if(name.charAt(i) == (char)invalidCharacters.get(j))
				{
				System.out.println("Invalid character detected!");
				System.out.println("Default name used instead!");
				this.myName = DEFAULT_NAME;
				}	
			}
		}
		
		//Blank names should not be allowed
		if(name == "")
			this.myName = DEFAULT_NAME;
		
		return false;
	}

	/**
	 * Just increases the score by one
	 */
	public void incrementScore() 
	{
		myNumWins++;
	  
	}
	
	public PieceType getPieceType()
	{
		return myPieceType;
	}

	/**
	 * Before getting the name this method validates it
	 * Setting it to a default one if there are spaces or 
	 * special characters.
	 * @return
	 */
	public String getName() 
	{
		validateName(myName);
		
		return this.myName;
	}

	public int getNumWins()
	{
		return myNumWins;
	}
	
	public int getScore()
	{
		return myNumWins;
	}
}