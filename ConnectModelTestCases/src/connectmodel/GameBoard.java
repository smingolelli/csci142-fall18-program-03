package connectmodel;

import java.awt.Point;
import java.util.Vector;
import connectmodel.PieceType;

/**
 * 
 * Connect 4 game board class, allows for piece placement
 * checking wins from all directions, getting win points,
 * and checking for the best place to put a piece.
 * 
 * It also checks if columns are full and if the board is full.
 * 
 * Finally. it can reset the board.
 * 
 * @author SMING, Daniel Plante
 *
 */

public class GameBoard {
    private int myNumRows;
    private int myNumColumns;
    private PieceType[][] myBoard;
    private int myNumTypes;
    private int myWinLength;
    private Point myLastPoint;
    private Vector<PieceType> myTypes;
    private Point myWinBegin;
    private Point myWinEnd;
    private boolean myIsAWin = false;
    private int y = -1;

    Point[] myColCounter = new Point[10];

    public GameBoard(int rows, int cols, int winLength, PieceType[] types)
    {
        myNumRows = rows;

        myNumColumns = cols;

        myWinLength = winLength;
        
        PieceType[][] board = new PieceType[rows][cols];
        
        myBoard = board;

        myTypes = getTypes();
        
        myIsAWin = getIsAWin();

        types[0] = PieceType.RED;

        if(myNumRows > 1 && myNumColumns>1)
        types[1] = PieceType.BLACK;

        for (int i = 0; i < myColCounter.length; i++)
        {
            myColCounter[i] = new Point(i, y);
        }
        
       

    }

    /**
     * Takes a column number and a piecetype.
     * If the column is full or is an invalid column a piece cannot
     * be placed there.
     * If the piece is successfully placed then a counter of how
     * many pieces are in that column increases. 
     * 
     * This method also creates a last point based on the row counter
     * and column put in. It will also not allow null types.
     * 
     * @param col
     * @param type
     * @return boolean
     */
    
    public boolean placePiece(int col, PieceType type)

    {
    	getBoard();
    	
        Point counter = new Point(col,myNumRows-1);

        if(type == null)
        {
        	return false;
        }
        
        if(col > myNumColumns-1)
        {
            return false;
        }
        
        if(col<0)
        {
            return false;
        }
        
        
        
        if(myBoard[counter.y][counter.x] != null)
            return false;
        
        
        
        myColCounter[col].y++;
        
        Point lastPoint = new Point(col,myColCounter[col].y);
        
        
        this.myLastPoint = new Point(0,0);
        
        this.myBoard[myColCounter[col].y][col] = type;
        
        myLastPoint = lastPoint;
        
        
       checkVerticalWin();
       checkHorizontalWin();   
       checkDiagonalWin();
        
        return true;
    }

    /**
     * Uses a nested for loop to set each part of the board as null
     */
    public void resetBoard()
    {

        for(int i = 0; i <myNumColumns-1; i++)
        {
            
            for(int j = 0; j < myNumRows-1; j++)
            {
                
                
                this.myBoard[j][i] = null;
                
                
            }
        }
        
        if(myNumColumns == 1 && myNumRows == 1)
        {
        	myBoard[0][0] = null;
        }
        
      
    }

    /**
     * Checks if the board only has null pieces, if it does then 
     * there is no way it can be a win.
     * 
     * Then it proceeds to call checkWin methods, if any are true
     * then it is a win
     * 
     * @return boolean
     */
    public boolean checkIfWin()
    {

    	if(checkAllNull()==true)
    	{
    		return false;
    	}
    	
    	
    
       if(myIsAWin==true)
    	   return true;
      
        
        return false;
    }

    /**
     * Calls methods based on the put in piecetype.
     * 
     * Each called method checks the 'length' in 3 different ways.
     * The length is the amount of types with the same color, except
     * for blocking.
     * 
     * Firstly, if there would be a win, it will prioritize putting the
     * piece so that type wins.
     * 
     * If that cannot be done, it will check length to see if it can
     * place it so another type cannot win.
     * 
     * If neither of these occur it will put it in the column
     * with the greatest length.
     * 
     * The return is an integer for the column.
     * @param type
     * @return int
     */
    public int findBestMoveColumn(PieceType type)
    {
    	for(int i = 0; i <myNumColumns; i++)
    	{
    	countHorizontalLengthIfPiecePlaced(i, type);
    	}
        return 0;
    }

    /**
     * checks a column based on where the last piece was put.
     * 
     * If the type is the same up and down for a total length of 4
     * (or whatever the win length is) then it is a win.
     * 
     * @return boolean
     */
    private boolean checkVerticalWin()
    {

    	if(checkHorizontalWin()==false)
    	{
    	
        Point winCheck = new Point(myLastPoint.x,myLastPoint.y);
                
        int counter = 0;
        
        PieceType check;
        
        check = myBoard[myLastPoint.y][myLastPoint.x];
        
        boolean isAWin = false;
        
        //check = getPieceOnBoard(winCheck);
        
        
        if(myNumColumns==1 && myNumRows == 1)
        {
        	if(myBoard[0][0] != null)
        		return true;
        }
        
                
        for(int i = 0; i < myNumRows; i++)
        {
        	if(winCheck.y < 0)
    		{
    			break;
    		}
        	
        	if(check == myBoard[winCheck.y][winCheck.x])
        	{
        		
        		counter++;
        		winCheck.y--;
        		
        		 if(counter >= myWinLength)
              	{
        			 winCheck.y++;
        			 
              		myWinBegin = winCheck;
              		
              		myWinEnd = myLastPoint;
              		
              		isAWin = true;
              		
              		myIsAWin = isAWin;
              		
              		return true;
              	}
        		
        		
        	}else {
        		break;
        	}
        }
    	
        
        if(winCheck.y<0)
        {
        winCheck = new Point(myLastPoint.x,myLastPoint.y);
        
        counter = 1;
        
        
        for(int i = 0; i < myNumRows; i++)
        {
        
        	 if(winCheck.y > myNumRows-1)
      		{
      			break;
      		}
        	
        	if(check == myBoard[winCheck.y][winCheck.x])
        		
        		
        	{
        		
        		counter++;
        		winCheck.y++;
        		
        		 if(counter >= myWinLength)
              	{
        			 winCheck.y--;
        			 
              		myWinBegin = winCheck;
              		
              		myWinEnd = myLastPoint;
              		
              		isAWin = true;
              		
              		myIsAWin = isAWin;
              		
              		return true;
              	}
        		
        		
        	}else {
        		break;
        	}
        }
        }
        
    	}
    	
        
        
    return false;
    }

    
    /**
     * checks columns based on where the last piece was put.
     * 
     * If the type is the same left and right for a total length of 4
     * (or whatever the win length is) then it is a win.
     * 
     * @return boolean
     */
    private boolean checkHorizontalWin()
    {
        
        Point winCheck = new Point(myLastPoint.x,myLastPoint.y);

        int counter = 0;
        
        PieceType check;
        
        check = myBoard[myLastPoint.y][myLastPoint.x];
        
        boolean isAWin = false;
        
        //check = getPieceOnBoard(winCheck);
        
        
        for(int i = 0; i < myNumColumns; i++)
        {
        	if(winCheck.x < 0)
    		{
    			break;
    		}
        	
        	if(check == myBoard[winCheck.y][winCheck.x])
        	{
        		
        		counter++;
        		winCheck.x--;
        		
        		 if(counter >= myWinLength)
              	{
        			winCheck.x++;
        			 
              		myWinBegin = winCheck;
              		
              		myWinEnd = myLastPoint;
              		
              		isAWin = true;
              		
              		myIsAWin = isAWin;
              		
              		return true;
              	}
        		
        		
        	}
        }
              
    return false;
        
    }

    /**
     * checks columns based on where the last piece was put.
     * 
     * If the type is the same to the left or right diagonally with 
     * a length of 4(or whatever the win length is) then it is a win.
     * 
     * @return boolean
     */
    private boolean checkDiagonalWin()
    {
    	
    	if(checkVerticalWin()==false && checkHorizontalWin()==false)
    	{
 
    	Point winCheck = new Point(myLastPoint.x,myLastPoint.y);

        int counter = 0;
        
        PieceType check;
        
        check = getPieceOnBoard(winCheck);
        
        boolean isAWin = false;
        
        //check = getPieceOnBoard(winCheck);
        
        
        
        
        if(myNumColumns==1 && myNumRows == 1)
        {
        	if(myBoard[0][0] != null)
        		return true;
        }
        
        for(int i = 0; i < myNumColumns; i++)
        {
        	if(winCheck.y > myNumRows-1 || winCheck.x > myNumColumns-1)
     		{
     			break;
     		}
        	
        	if(check == myBoard[winCheck.y][winCheck.x])
        	{
        		
        		counter++;        		
        		winCheck.y++;
          		winCheck.x++;
          		
        		 if(counter >= myWinLength)
              	{
        			winCheck.x--;
        			
        			winCheck.y--;
        			 
        			 
              		myWinBegin = winCheck;
              		
              		myWinEnd = myLastPoint;
              		
              		isAWin = true;
              		
              		myIsAWin = isAWin;
              		
              		return true;
              	}
        		 
        		
        	}
        	
        	
        }
    	
        
    	}
    	
    	
    	
    return false;
    }

    
    /**
     * Method that takes a type and column number.
     * It then checks multiple columns left and right for length of 
     * the same or different type. If there is a win it should be put into
     * that column. If it would stop another player from winning
     * it should put it into that column. If neither of these
     * apply then it will put it in the column with the most identical
     * piece types from all directions
     * @param col
     * @param type
     * @return int
     */
    private int countHorizontalLengthIfPiecePlaced(int col, PieceType type)
    {
    	Point checker = new Point(col,myLastPoint.y);
    	
    	
    	int lengthCount = 0;
    	
    	
    	
    	for(int i = col; i > 0; i--) 
    	{
    		if(getPieceOnBoard(checker) == type)
    		{
    			lengthCount++;
    			checker.x--;
    		}
    		
    		if(checker.x == 0)
    		{
    			break;
    		}
    	}
    	
    	
    	for(int i = col; i < myNumColumns++; i++) 
    	{
    		if(getPieceOnBoard(checker) == type)
    		{
    			lengthCount++;
    			checker.x++;
    		}
    		if(checker.x == myNumColumns-1)
    			break;
    	}
    	
    	return lengthCount;
    }
    	
    	
    
    
    
    /**
     * Method that takes a type and column number.
     * It then checks multiple columns up and down for length of 
     * the same or different type. If there is a win it should be put into
     * that column. If it would stop another player from winning
     * it should put it into that column. If neither of these
     * apply then it will put it in the column with the most identical
     * piece types from all directions
     * @param col
     * @param type
     * @return int
     */
    private int countVerticalLengthIfPiecePlaced(int col, PieceType type)
    {
    	
Point checker = new Point(col,myLastPoint.y);
    	
    	
    	int lengthCount = 0;
    	
    	
    	
    	for(int i = col; i > 0; i--) 
    	{
    		if(getPieceOnBoard(checker) == type)
    		{
    			lengthCount++;
    			checker.y--;
    		}
    		
    		if(checker.y == 0)
    		{
    			break;
    		}
    	}
    	
    	
    	for(int i = col; i < myNumRows++; i++) 
    	{
    		if(getPieceOnBoard(checker) == type)
    		{
    			lengthCount++;
    			checker.y++;
    		}
    		if(checker.y == myNumRows-1)
    			break;
    	}
    	
    	return lengthCount;
    	
        
    }

    
    /**
     * Method that takes a type and column number.
     * It then checks multiple columns diagonally left and right for 
     * length of the same or different type. If there is a win 
     * it should be put into that column. If it would stop another player 
     * from winning it should put it into that column. If neither of these
     * apply then it will put it in the column with the most identical
     * piece types from all directions
     * @param col
     * @param type
     * @return int
     */
    private int countDiagonalLengthIfPiecePlaced(int col, PieceType type) {
        return 0;
    }

    public Vector<PieceType> getTypes() {
        Vector<PieceType> v = new Vector<PieceType>(4);

        v.add(PieceType.BLACK);

        v.add(PieceType.GREEN);

        v.add(PieceType.YELLOW);

        v.add(PieceType.RED);

        myTypes = v;

        return myTypes;

    }

    
    
    public Point getWinBegin()
    {
        
        if(myIsAWin==true)
        return myWinBegin;
        
        return null;

    }

    public Point getWinEnd()
    {
        if(myIsAWin==true)
        return myWinEnd;
        
        return null;
    }

    public Point getLastPoint() 
    {
        return myLastPoint;
    }

    
    /**
     * Returns a type based on the points given.
     * It checks the board with a y point and an x point.
     * @param point
     * @return PieceType
     */
    public PieceType getPieceOnBoard(Point point) 
    {


        PieceType temp = myBoard[point.y][point.x];

        return temp;

    }

    public PieceType[][] getBoard() {
        PieceType[][] board = new PieceType[myNumRows][myNumColumns];

        return board;
    }

    /**
     * Checks if all the columns are full, if they are all full
     * then the board is full.
     * @return boolean
     */
    public boolean isBoardFull() {
        int colCounter = 0;

        for (int i = 0; i < myNumColumns; i++) {

            if (isColumnFull(i) == true) {
                System.out.println("Column: " + i + " is full.");

                colCounter++;
            }

        }

        if (colCounter == myNumColumns) {
            System.out.println("The board is full!");
            return true;
        }

        return false;
    }

    /**
     * If the column has a piece at its highest point
     * then it is full.
     * @param col
     * @return
     */
    public boolean isColumnFull(int col)
    {
    	
    	Point counter = new Point(col,myNumRows-1);
        
        if(myBoard[counter.y][counter.x] != null)
        {
            return true;
        }
        
        
        return false;
    }

    public boolean getIsAWin() {

        
        return myIsAWin;
    }

    /**
     * If any spot on the board is not null, then not all
     * spots on the board are null
     * @return boolean
     */
    public boolean checkAllNull() {
    	
    	
    	if(myNumColumns == 1 && myNumRows == 1)
        {
        	if (myBoard[0][0] != null)
        		return false;
        }
    	
    	for(int i = 0; i < myNumColumns-1; i++)
    	{
    		if(myBoard[0][i]!=null)
    			return false;
    	}
    	
    	
        
        return true;
    }
}