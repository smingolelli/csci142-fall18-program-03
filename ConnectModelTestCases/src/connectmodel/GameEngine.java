package connectmodel;

import java.util.Objects;
import java.util.Vector;

/**
 * The Game Engine class carries out the functions necessary for 
 * getting the current player, changing between players,
 * placing pieces, as well as starting the game.
 * 
 * @author SMING, Daniel Plante
 *
 */


public class GameEngine 
{
	private Vector<Player> myPlayers;
	private Player myPlayerUp;
	private Player myStartingPlayer;
	private GameBoard myGameBoard;
  
	public GameEngine(Player player, GameBoard myGameBoard) 
	{
		
		
		PieceType[] pieceTypes = new PieceType[2];
		pieceTypes[0] = PieceType.BLACK;
		pieceTypes[1] = PieceType.RED;

		this.myGameBoard = new GameBoard(6,7,4,pieceTypes);

		myPlayers = getPlayers();
		
		myStartingPlayer = myPlayers.firstElement();
		
		myPlayerUp = player;
		
		myGameBoard = getGameBoard();
		
	  Objects.requireNonNull(this.myGameBoard);
	  
	 
	}


	public boolean selectStartingPlayer(Player player)
	{
		
		if(player.getName()!=null)
			return false;
		
		if(getGameBoard() == null)
		{
			return false;
		}
		
		if(myGameBoard == null)
		{
			return false;
		}
		
		getPlayers();
		
		myStartingPlayer = player;
		
		this.myStartingPlayer = myPlayers.firstElement();
		
		return true;
	}

	/**
	 * Resets the board first, won't allow nulls
	 */
	public boolean startGame() 
	
	{


		
		if(Objects.equals(myGameBoard, null))
			return false;
		
			myGameBoard.resetBoard();
			
		
			if(myPlayerUp==null)
				return false;

		
		return true;
	}
	/**
	 * If the first player in the vector is up, then the second player
	 * in the vector becomes the new current player and vice versa.
	 * @return
	 */
	public Player switchPlayerUp() 
	{
		
		if(myPlayers.firstElement() == myPlayerUp)
		{
			myPlayerUp = myPlayers.lastElement();
		}
		
		if(myPlayers.lastElement() == myPlayerUp)
		{
			myPlayerUp = myPlayers.firstElement();
		}
		
		return this.myPlayerUp;
	}

	/**
	 * Takes a column number and then calls the board so it 
	 * places a piece based on the current players type.
	 * @param column
	 * @return boolean
	 */
	public boolean placePiece(int column)
	{
		
		if(startGame() == true)
		{
			
			getGameBoard();
			
			myGameBoard.getBoard();
			
			myGameBoard.placePiece(column, myPlayerUp.getPieceType());
			
			
			
			return true;
		}
		
			
		
		return false;
	}

	public Player getPlayerUp() 
	
	{
		
		
		return myPlayers.firstElement();
	}
	
	public Player getStartingPlayer()
	{
		return myStartingPlayer;
	}

	public Vector<Player> getPlayers()
	{
		
		 Player john = new Player(Player.DEFAULT_NAME,PieceType.YELLOW);
			
		Vector<Player> players = new Vector<Player>(2,1);
		
		players.add(john);
		
		john.getName();
		
		john.getPieceType();
		
		this.myPlayers = players;
		
		Player computer = new Player("botJohn",PieceType.GREEN);
		
		myPlayers.add(computer);
		
		return this.myPlayers;
	}
  /**
   * Resets the gameboard first and makes sure all is null.
   * @param gameboard
   */
	public void setGameBoard(GameBoard gameboard)
	{
		
		
		gameboard.resetBoard();
		
		gameboard.checkAllNull();
		
		
		Objects.requireNonNull(gameboard);
		
	}

	public GameBoard getGameBoard()
	{
		
		
		return myGameBoard;
	}
	
}